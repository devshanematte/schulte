import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers';
import * as serviceWorker from './serviceWorker';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { createStore } from './services';
import { Route, HashRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import './services/styles/game.css';

const {
	store,
	persistor
} = createStore();

ReactDOM.render(
  	<React.StrictMode>
	  	<Provider store={store}>
	    	<PersistGate loading={null} persistor={persistor}>
	      		<HashRouter>
	        		<Route path="/" render={() => <App /> } />
	      		</HashRouter>
	    	</PersistGate>
		</Provider>
  	</React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
