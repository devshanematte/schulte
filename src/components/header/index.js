import React, {
	useState
} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { backgrounds } from '../../domain';

const Header = () => {

	const dispatch = useDispatch();

	const [settingsModal, setSettingsModal] = useState(false);

	let gameState = useSelector(state => state.game);
	let appState = useSelector(state => state.app);
	const points = gameState.get('points');
	const selectedBackground = appState.get('background');

	return (
		<div className="row">

			<div className={settingsModal ? "settings-modal settings-modal-opened" : "settings-modal"}>
				<div className="settings-modal-content">
					<h2>Выберите картинку на фон</h2>
					<div className="settings-modal-content-images">
						{
							backgrounds.map((item, index)=>{

								const selectImage = index == selectedBackground ? 'background-item selected-image' : 'background-item';

								return (
									<div className={selectImage} onClick={()=>{
										setSettingsModal(!settingsModal);
										return dispatch({
											type:'UPDATE_BACKROUND_GAME',
											background:index
										});
									}} style={{
										background:`url(${item.background}) center / cover no-repeat`
									}}></div>
								)
							})
						}
					</div>
				</div>
			</div>

			<header>
				<div className="col-md-4">
					<div className="row">
						<h1>Таблицы шульте</h1>
					</div>
				</div>
				<div className="col-md-8">
					<div className="row">
						<section className="header-right">
							<section>
								<span onClick={()=>{ setSettingsModal(!settingsModal) }} className="theme-button"></span>
							</section>
							<section>
								<span className="points-button"></span>
								<p>{ points }</p>
							</section>
						</section>
					</div>
				</div>
			</header>
		</div>
	)

}

export default Header;