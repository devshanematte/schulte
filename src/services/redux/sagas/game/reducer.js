import { 
	Record, 
	Map
} from 'immutable';

import { defaultGames } from '../../../../domain';

let {
	classic,
	rus, 
	eng	
} = defaultGames();

let initialState = new Record({
	points:0,
	statusGame:'SELECT_GAME',
	typeGame:null,

	indexNumber:0,

	CLASSIC:classic.sort(),
	RUS:rus.sort(),
	ENG:eng.sort()
})();

const game = (state = initialState, action) => {
	switch(action.type){
		
		case 'STATUS_GAME_ACTION' :

			return state.withMutations((state)=>{
				state.set('statusGame', action.status);
			});

		case 'SELECT_GAME_TYPE_ACTION' :

			return state.withMutations((state)=>{
				state.set('typeGame', action.typeGame);
			});

		case 'SELECT_NUMBER_GAME_ACTION' :

			return state.withMutations((state)=>{
				state.set('indexNumber', action.index);
			});

		default :
			return state;

	}
}

export default game;