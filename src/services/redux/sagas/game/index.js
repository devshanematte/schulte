
import { put, take, takeEvery, call, select } from 'redux-saga/effects';

function* updateGamePoints(action) {

	yield put({
		type:'UPDATE_GAME_POINTS_ACTION',
		points:action.points
	});

}

function* selectGameType(action) {

	yield put({
		type:'STATUS_GAME_ACTION',
		status:action.status
	});

	yield put({
		type:'SELECT_GAME_TYPE_ACTION',
		typeGame:action.typeGame
	});

}

function* resetStatusGame(action) {

	yield put({
		type:'STATUS_GAME_ACTION',
		status:'SELECT_GAME'
	});

	yield put({
		type:'SELECT_GAME_TYPE_ACTION',
		typeGame:null
	});

}

function* selectGameNumber(action) {

	yield put({
		type:'SELECT_NUMBER_GAME_ACTION',
		index:action.index
	});

}

function* gameSagas() {
  	yield takeEvery('UPDATE_GAME_POINTS', updateGamePoints);
  	yield takeEvery('SELECT_GAME_TYPE', selectGameType);
  	yield takeEvery('RESET_STATUS_GAME', resetStatusGame);
  	yield takeEvery('SELECT_NUMBER_GAME', selectGameNumber);
}

export default gameSagas;