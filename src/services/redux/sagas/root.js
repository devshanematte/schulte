import { fork, all } from 'redux-saga/effects';

import appSagas from './app';
import gameSagas from './game';

export default function* rootSaga() {
  	yield all([
    	fork(appSagas),
    	fork(gameSagas)
  	]);
}