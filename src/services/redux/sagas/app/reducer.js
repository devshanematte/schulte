import { 
	Record, 
	Map,
	fromJS
} from 'immutable';

let initialState = new Record({
	version:'0.0.1',
	background:4
})();

const app = (state = initialState, action) => {
	switch(action.type){

		
		case 'UPDATE_BACKROUND_GAME_ACTION' :

			return state.withMutations((state)=>{
				state.set('background', action.background);
			});
		
		default :
			return state;

	}
}

export default app;