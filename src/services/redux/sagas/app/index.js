
import { put, take, takeEvery, call, select } from 'redux-saga/effects';

function* updateVersionApp(action) {

	yield put({
		type:'UPDATE_VERSION_APP_NUMBER',
		version:action.version
	});

}

function* updateBackgroundGame(action) {

	yield put({
		type:'UPDATE_BACKROUND_GAME_ACTION',
		background:action.background
	});

}

function* appSagas() {
  	yield takeEvery('UPDATE_VERSION_APP', updateVersionApp);
  	yield takeEvery('UPDATE_BACKROUND_GAME', updateBackgroundGame);
}

export default appSagas;