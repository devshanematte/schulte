import { applyMiddleware, compose, createStore } from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas/root';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import immutableTransform from 'redux-persist-transform-immutable';

export default (reducers) => {

  const persistConfig = {
    transforms: [immutableTransform()],
    key: 'root',
    storage: storage,
    blacklist:['game']
  }

  const middleware = [];
  const enhancers = [];

  const sagaMiddleware = createSagaMiddleware();

  middleware.push(logger);
  middleware.push(sagaMiddleware);

  enhancers.push(applyMiddleware(...middleware));

  const pReducers = persistReducer(persistConfig, reducers);

  const store = createStore(pReducers, compose(...enhancers));
  const persistor = persistStore(store);

  sagaMiddleware.run(rootSaga);

  return {
    store,
    persistor
  }

}
