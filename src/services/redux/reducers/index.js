import { combineReducers } from 'redux';

import app from '../sagas/app/reducer';
import game from '../sagas/game/reducer';

export default combineReducers({
	app,
	game
});