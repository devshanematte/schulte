import store from './store';
import reducers from './reducers';

export default () => {
  return store(reducers);
}
