import React, {
	useEffect
} from 'react';

import {
	Header
} from '../../components';

import { useDispatch } from 'react-redux';

import SelectGame from './Helpers/selectGame';
import Table from './Helpers/table';

const Game = () => {

	const dispatch = useDispatch();

	useEffect(()=>{

	}, []);

	return (
		<div>
			<Header/>
			<SelectGame/>
			<Table/>
		</div>
	)
}

export default Game;