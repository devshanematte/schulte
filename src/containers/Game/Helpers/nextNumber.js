import React, {
	useEffect, 
	useState
} from 'react';
import { useSelector } from 'react-redux';

const NextNumber = () => {

	const [numbers, setNumbers] = useState(null);

	const gameState = useSelector(state => state.game);

	const typeGame = gameState.get('typeGame');
	const classicGame = gameState.get('CLASSIC');
	const rusGame = gameState.get('RUS');
	const engGame = gameState.get('ENG');

	const indexNumber = gameState.get('indexNumber');

	useEffect(() => {

		updateNumbers(typeGame);

	}, [typeGame]);

	const updateNumbers = (type) => {

		if(type == 'CLASSIC') {
			
			setNumbers(classicGame);
		}

		if(type == 'RUS') {
			setNumbers(rusGame);
		}

		if(type == 'ENG') {
			setNumbers(engGame);
		}

		return

	}

	return(
		<div className="game-block-header">
			<h1>{ numbers ? numbers[indexNumber].title : '' }</h1>
		</div>
	)
}

export default NextNumber;