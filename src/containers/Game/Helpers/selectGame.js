import React from 'react';

import { useSelector, useDispatch } from 'react-redux';

const SelectGame = () => {

	const dispatch = useDispatch();
	const gameState = useSelector(state => state.game);

	const gameStatusStyle = gameState.get('statusGame') == 'SELECT_GAME' ? 'select-game-block' : 'select-game-block select-game-block-closed';

	const selectGame = (type) => {
		return dispatch({
			type:'SELECT_GAME_TYPE',
			status:'GAME',
			typeGame:type
		});
	}


	return (
		<div className="row">
			<div className={gameStatusStyle}>
				<section onClick={()=>{selectGame('CLASSIC')}}>
					<p>Классика</p>
				</section>
				<section onClick={()=>{selectGame('RUS')}}>
					<p>Русский алфавит</p>
				</section>
				<section onClick={()=>{selectGame('ENG')}}>
					<p>Английский алфавит</p>
				</section>
			</div>
		</div>
	)

}

export default SelectGame;