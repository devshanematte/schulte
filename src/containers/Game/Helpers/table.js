import React, {
	useState,
	useEffect,
	useMemo
} from 'react';

import { useSelector, useDispatch } from 'react-redux';
import { typeGames } from '../../../domain';

import NextNumber from './nextNumber';

const {
	classic,
	rus, 
	eng
} = typeGames();

const Table = () => {

	const dispatch = useDispatch();
	const [numbers, setNumbers] = useState(null);

	const gameState = useSelector(state => state.game);
	const statusGame = gameState.get('statusGame') == 'SELECT_GAME' ? true : false;
	const typeGame = gameState.get('typeGame');
	const indexNumber = gameState.get('indexNumber');

	useEffect(() => {

		updateNumbers(typeGame);

	}, [typeGame]);

	const randomNumbers = (data) => {
		
		setNumbers(data.sort(()=>{
			return 0.5 - Math.random();
		}));

	}

	const updateNumbers = (type) => {

		if(type == 'CLASSIC') {
			randomNumbers(classic);
		}

		if(type == 'RUS') {
			randomNumbers(rus);
		}

		if(type == 'ENG') {
			randomNumbers(eng);
		}

		return

	}

	const hundleNumber = () => {

		let incNumberIndex = Number(indexNumber) + 1;

		if(incNumberIndex < numbers.length){
			return dispatch({
				type:'SELECT_NUMBER_GAME',
				index:incNumberIndex
			});
		}

		return alert('End game');

	}

	return (
		<div className="row">
			<div className="table-game">
				{
					statusGame ?
						<div className="select-game-text-default">
							<h4>Выберите режим игры</h4>
						</div>
					: numbers ?
						<div className="full-container-block">
							<NextNumber/>
							<div className="table-game-content">
								{
									numbers.map((item, index)=>{
										return (
											<div onClick={()=>{hundleNumber(item.title)}} className="item-button-game" key={index}>
												<h5>{item.title}</h5>
											</div>
										)
									})
								}
							</div>
						</div>
					:
						<div></div>
				}
			</div>
		</div>
	);

}

export default Table;