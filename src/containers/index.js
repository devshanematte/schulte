import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';
import { useSelector } from 'react-redux';
import { backgrounds } from '../domain';

import 'react-notifications/lib/notifications.css';

import Game from './Game';

const Initial = () => {

	const appState = useSelector(state => state.app);
	const backgroundApp = appState.get('background') ? appState.get('background') : 0;

	return (

		<div className="main-app-content" style={{
			background:`url(${backgrounds[backgroundApp].background}) center / cover no-repeat`
		}}>
			<div className="container">
				<Switch>
					<Route exact path='/' render={(props)=> <Game {...props}/>}/>

					{/*Redirect if 404*/}
					<Route path='*' render={()=> <Redirect to="/" />}/>
			  	</Switch>

			  	<NotificationContainer/>

			</div>
		</div>

	)
}

export default Initial;