import backgrounds from './backgrounds';
import typeGames from './typeGames';
import defaultGames from './defaultGames';

export {
	backgrounds,
	typeGames,
	defaultGames
}