import classic from './classic';
import rus from './rus';
import eng from './eng';

export default () => {
	return {
		classic,
		rus, 
		eng	
	}
}